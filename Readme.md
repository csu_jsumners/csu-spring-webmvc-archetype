# csu-spring-webmvc Archetype #

This project provides the necessary structure to create a Maven archetype. This archetype generates a minimal annotation based [Spring](http://spring.io/) web application.

## Usage ##

### CLI Maven ###

To use the archetype from the command line Maven interface, you simply specify the CSU repository when issuing a `archetype:generate`. For example:

```
mvn archetype:generate -DarchetypeCatalog="http://auryn.clayton.edu:8081/nexus/content/repositories/edu.clayton"
```

### IntelliJ ###

When creating a new project in IntelliJ, and specifying the "Maven module", you can simply fill in the [add archetype dialog](http://www.jetbrains.com/idea/webhelp/add-archetype-dialog.html) with the following values:

1. GroupId = "edu.clayton"
2. ArtifactId = "csu-spring-webmvc"
3. Version = "RELEASE" (or "0.3")
4. Repository = "http://auryn.clayton.edu:8081/nexus/content/repositories/edu.clayton"
